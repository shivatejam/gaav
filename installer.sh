#!/bin/bash


set -e

# Defaults ------------------------------------------------------
MINICONDA_DIR="${HOME}/miniconda"
INSTALL_DIR="${HOME}/ngstools"
TO_INSTALL="bwa,samtools,bcftools"
INSTALL_MINICONDA=1
ENVNAME="gaav-test"
LOG=/dev/stdout
VERBOSE=0
GIT_TAG=""


USE_BWA_VERSION=0.7.10
USE_SAMTOOLS_VERSION=1.0
USE_BCFTOOLS_VERSION=1.0
MINICONDA_VERSION=3.7.0

# ---------------------------------------------------------------

while getopts "hd:i:m:Me:vg" x; do
    case "$x" in
        h)
            echo "
usage: $0 [options] | check

    This script helps install all the necessary prerequisites for running
    gaav.

    When the single argument \"check\" is provided, this script simply prints
    any gaav prerequisites it finds on the path.  Example usage:

        $0 check

    Otherwise:

    This script can install the prerequisite ngs tools (bwa,
    samtools, bcftools) necessary for running gaav (genome assembly automatic validation).  It can also install
    a standalone Python installation (Miniconda, a more streamlined version of
    Anaconda; more info at http://docs.continuum.io/anaconda/index.html) that
    includes gaav and all its Python requirements.  This can be used
    alongside any other Python versions that may be installed.

    By default, this script will:

        1) Download and install bwa, samtools, bcftools.  It will place them in the
           following directory:

              ${INSTALL_DIR}

        2) Download and install Miniconda into the following directory:

              ${MINICONDA_DIR}

        3) Create an isolated environment, located at:

              ${HOME}/miniconda/envs/${ENVNAME}

        4) Install Python dependencies for gaav in this environment

        5) Install the latest gaav release in this environment

    Further instructions are displayed upon successful installation.

    Options
    -------

    -h           Print this help message and exit.

    -d DIR       Directory where pre-requisistes will be installed.
                 Default is ${INSTALL_DIR}.

    -i STRING    A string (usually comma-separated with no spaces) containing
                 the names of prerequisite tools to install in DIR.  Options
                 are bwa, samtools, bcftools. Default is
                 \"${TO_INSTALL}\".

    -m DIR       Directory where miniconda (an isolated python distribution) will be installed.
                 Default is ${MINICONDA_DIR}.

    -e ENVNAME   Name of new environment that will be created with all the
                 Python dependencies.  Default is ${ENVNAME}.

    -M           Disable installation of Miniconda.  This option is best if you
                 want to use an existing installation of the scientific Python
                 stack (scipy, numpy, matplotlib). However, even if
                 you already have these installed, a Miniconda installation is
                 completely separate and so -M is not required.  It just saves
                 some setup time.

    -v           Verbose mode.  By default, the stdout and stderr from each
                 installation step will be written to a separate log file.  If
                 -v is used, then all output will be written to stdout.

    -g TAG       This script installs the latest gaav release using git 
                 Use the -g flag to specify a git tag from the gaav bitbucket
                 repository.  The latest version can be accessed using the
                 'master' tag. Use the special tag 'disable' to prevent gaav
                 from being installed at all.


    The ngs tool versions can be changed by editing the
    'USE_<tool>_VERSION' variables at the top of this script. The versions
    currently configured are:

        bwa           : ${USE_BWA_VERSION}
        samtools      : ${USE_SAMTOOLS_VERSION}
        bcftools      : ${USE_BCFTOOLS_VERSION}


    Example usage:

        # \"The works\": download and install all ngs tools as well as an
        # isolated environment.  Depending on your hardware and internet
        # connection speed, this could take 5 minutes or more.

            $0

        # Already have bwa, samtools, bcftools and just want
        # to create an isolated environment in 'my-env'

            $0 -i \"\" -e my-env


        # Install the ngs tools without making a new isolated environment:

            $0 -M

        # Just want the prerequisites and an isolated environment but don't
        # want to install gaav?

            $0 -g disable

        # Output *everything* to the terminal instead of saving to separate
        # logs.  This generates lots of output but it can be useful for
        # debugging:

            $0 -v


            "
            exit 2
            ;;
        d)
            INSTALL_DIR="${OPTARG}"
            ;;
        e)
            ENVNAME="${OPTARG}"
            ;;
        i)
            TO_INSTALL="${OPTARG}"
            ;;
        m)
            MINICONDA_DIR="${OPTARG}"
            ;;
        M)
            INSTALL_MINICONDA=0
            ;;
        g)
            GIT_TAG="${OPTARG}"
            ;;
        v)
            VERBOSE=1
            ;;

        ?)
            echo "did not recognize option, try -h"
            exit 1
            ;;
    esac
done

# Logs first arg to stdout, with a timestamp
log ()
{
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] : $1"
}


# Mac or Linux?
uname | grep "Darwin" > /dev/null && SYSTEM_TYPE=mac || SYSTEM_TYPE=linux
log "Detected operating system: ${SYSTEM_TYPE} (based on the output of uname, which is \"$(uname)\")".

MACH_TYPE=$(uname -m)

if [[ ($MACH_TYPE != "x86_64") && ($SYSTEM_TYPE = "mac") && ( $INSTALL_MINICONDA = 1 ) ]]; then
    echo "
    Sorry, installing miniconda on 32-bit Mac OSX is not supported.  Please see
    http://conda.pydata.org/miniconda.html.  Exiting!
    "
    exit 1
fi

# Determine how to download files.  wget is (typically) installed by default on
# Linux; curl on Mac.
if [[ $SYSTEM_TYPE = "mac" ]]; then
    downloader ()
    {
        log "Downloading $1"
        curl --location $1 > $2
    }
else
    downloader ()
    {
        log "Downloading $1"
        wget $1 -O $2
    }
fi

# Functions to check installation --------------------------------------------
check_bwa () {
    command -v bwa > /dev/null 2>&1 \
        && echo "yes [ $(bwa 2>&1 > /dev/null | grep Version) ] $(which bwa)" \
        || echo "no"
}

check_samtools () {
    command -v samtools > /dev/null 2>&1 \
        && echo "yes [ $(samtools 2>&1 > /dev/null | grep Version) ] $(which samtools)" \
        || echo "no"
}

check_bcftools () {
    command -v bcftools > /dev/null 2>&1 \
        && echo "yes [ $(bcftools 2>&1 > /dev/null | grep Version) ] $(which bcftools)" \
        || echo "no"
}

check_gcc () {
    command -v gcc > /dev/null 2>&1 \
        && echo "yes [ $(gcc --version 2> /dev/null | grep -E 'version|gcc') ] $(which gcc)" \
        || echo "no"
}

check_python () {
    command -v python > /dev/null 2>&1 \
        && echo "yes [ $(python --version 2>&1) ] $(which python)"  \
        || echo "no"
}

check_git () {
    command -v git > /dev/null 2>&1 \
        && echo "yes [ $(git --version) ] $(which git)" \
        || echo "no"
}

check_python_package () {
    pkg="$1"
    vsn () {
        (cd $HOME && python -c "import $pkg; print ${pkg}.__version__")
    }

    pth () {
        (cd $HOME && python -c "import $pkg, os; print os.path.dirname(${pkg}.__file__)")
    }
    vsn > /dev/null 2>&1 \
        && echo "yes [ $(vsn) ] $(pth)" \
        || echo "no"
}

check_all () {

    echo "
    Compilers:
        gcc              : $(check_gcc)
    
    DVCS:
       git               : $(check_git)

    Ngs tools:
	bwa              : $(check_bwa)
        samtools         : $(check_samtools)
        bcftools         : $(check_bcftools)
        
    Python:
        python           : $(check_python)
        scipy            : $(check_python_package scipy)
        numpy            : $(check_python_package numpy)
    "
}

if [[ "$1" == "check" ]]; then
    check_all
    exit 0
fi

# Path and log file setup ------------------------------------------

# Make the install dir and convert to absolute path
mkdir -p "${INSTALL_DIR}"
INSTALL_DIR=$(cd "${INSTALL_DIR}"; pwd)

# Paths for individual tools
BWA_PATH="${INSTALL_DIR}/bwa"
SAMTOOLS_PATH="${INSTALL_DIR}/samtools-${USE_SAMTOOLS_VERSION}"
BCFTOOLS_PATH="${INSTALL_DIR}/bcftools-${USE_BCFTOOLS_VERSION}"

# Set up log files (or just stdout if requested)
if [[ $VERBOSE = 1 ]]; then
    BWA_INSTALL_LOG=/dev/stdout
    SAMTOOLS_INSTALL_LOG=/dev/stdout
    BCFTOOLS_INSTALL_LOG=/dev/stdout
    MINICONDA_INSTALL_LOG=/dev/stdout
    ENV_INSTALL_LOG=/dev/stdout
    GAAV_INSTALL_LOG=/dev/stdout
else
    LOG_DIR="${INSTALL_DIR}/logs"
    mkdir -p "${LOG_DIR}"    
    BWA_INSTALL_LOG="${LOG_DIR}/bwa-installation.log"
    SAMTOOLS_INSTALL_LOG="${LOG_DIR}/samtools-installation.log"
    BCFTOOLS_INSTALL_LOG="${LOG_DIR}/bcftools-installation.log"
    MINICONDA_INSTALL_LOG="${LOG_DIR}/miniconda-installation.log"
    ENV_INSTALL_LOG="${LOG_DIR}/miniconda-environment-installation.log"
    GAAV_INSTALL_LOG="${LOG_DIR}/gaav-installation.log"
fi

# Keep track of installations we found before running the script.
INITIAL=$(check_all)

cat <<EOF
Found the following prerequisites on the path:

$INITIAL

EOF

# System-specific error messages ----------------------------------------
#
#   This is too be displayed when gcc and gfortran is unavailable.
gcc_error_message () {
    if [[ $SYSTEM_TYPE = "mac" ]]; then
        cat <<EOF

    gcc is required to compile bwa, samtools and bcftools.  On Mac, you need to install
    the correct version of Xcode (https://developer.apple.com/xcode/downloads/)
    for your version of Mac OSX.

    Please install gcc and then re-run this script.

    Exiting!

EOF
    else
        cat <<EOF

    gcc is required to compile bwa, samtools and bcftools.  On Ubuntu, please use:

        sudo apt-get install build-essential

    and then re-run this script.

    Exiting!

EOF
    fi
}

tailinfo () {

    if [[ $VERBOSE = 1 ]]; then
        echo ""
    else
        echo "To follow progress, paste the command 'tail -f $1' without the quotes in another terminal."
    fi

}
# Installation functions -----------------------------------------------------
#
#   Each function installs the prerequisite into a subdirectory of
#   $INSTALL_DIR.  The subdirectory is named after the tool name and version
#   (see top of this script for version info).
#
#   Each function also writes the installed path $INSTALL_DIR/paths.  At the
#   end of this script, this file is sourced to show that programs have been
#   found; it also provides a single reference for paths to add to $PATH.
#
#   Functions may check for the presence of gcc as needed.
#
# ----------------------------------------------------------------------------

# Ensure we start with fresh copy of the paths file.
cat /dev/null > "${INSTALL_DIR}/paths"
echo "# Added by gaav installation, $(date)" >> "${INSTALL_DIR}/paths"

install_bwa () {
    # Download and install bwa
    if [[ check_gcc = "no" ]]; then
        gcc_error_message
        exit 1
    fi
    mkdir -p ${BWA_PATH}
    ARCHIVE="${INSTALL_DIR}/bwa-${USE_BWA_VERSION}.tar.bz2"
    (
        downloader  "http://downloads.sourceforge.net/project/bio-bwa/bwa-${USE_BWA_VERSION}.tar.bz2"  ${ARCHIVE} \
    && tar -xjf "${ARCHIVE}" --directory ${BWA_PATH} --strip-components=1 \
    && cd "${BWA_PATH}" \
    && make \
    && rm ${ARCHIVE} \
    && echo "export PATH=\$PATH:${BWA_PATH}" >> "${INSTALL_DIR}/paths"
    )
}

install_samtools () {
    # Download and install samtools
    if [[ check_gcc = "no" ]]; then
        gcc_error_message
        exit 1
    fi
    mkdir -p ${SAMTOOLS_PATH}
    ARCHIVE="${INSTALL_DIR}/samtools-${USE_SAMTOOLS_VERSION}.tar.bz2"
    (
        downloader "https://downloads.sourceforge.net/project/samtools/samtools/${USE_SAMTOOLS_VERSION}/samtools-${USE_SAMTOOLS_VERSION}.tar.bz2" ${ARCHIVE} \
    && tar -xjf "${ARCHIVE}" --directory ${SAMTOOLS_PATH} --strip-components=1 \
    && cd "${SAMTOOLS_PATH}" \
    && make \
    && rm ${ARCHIVE} \
    && echo "export PATH=\$PATH:${SAMTOOLS_PATH}" >> "${INSTALL_DIR}/paths"
    )
}

install_bcftools () {
    # Download and install bcftools
    if [[ check_gcc = "no" ]]; then
        gcc_error_message
        exit 1
    fi
    mkdir -p ${BCFTOOLS_PATH}
    ARCHIVE="${INSTALL_DIR}/bcftools-${USE_BCFTOOLS_VERSION}.tar.bz2"
    (
        downloader "https://downloads.sourceforge.net/project/samtools/samtools/${USE_BCFTOOLS_VERSION}/bcftools-${USE_BCFTOOLS_VERSION}.tar.bz2" ${ARCHIVE} \
    && tar -xjf "${ARCHIVE}" --directory ${BCFTOOLS_PATH} --strip-components=1 \
    && cd "${BCFTOOLS_PATH}" \
    && make \
    && rm ${ARCHIVE} \
    && echo "export PATH=\$PATH:${BCFTOOLS_PATH}" >> "${INSTALL_DIR}/paths"
    )
}

# NGS tools installation ---------------------------------------
# Here we're just checking for the presence of these strings in the
# $TO_INSTALL var.
#
# bwa installation --------------------------------------------------
echo "${TO_INSTALL}" | grep "bwa" > /dev/null \
    && {
         log "Installing bwa version ${USE_BWA_VERSION} to ${BWA_PATH}.  $(tailinfo ${BWA_INSTALL_LOG})" \
         && { [[ check_gcc = "no" ]] && { gcc_error_message; exit 1; } } \
         || install_bwa > $BWA_INSTALL_LOG 2>&1 && log "Done, see ${BWA_INSTALL_LOG}.";
     } || log "skipping bwa installation"

# samtools installation ----------------------------------------------
echo "${TO_INSTALL}" | grep "samtools" > /dev/null \
    && {
        log "Installing samtools version ${USE_SAMTOOLS_VERSION} to ${SAMTOOLS_PATH}. $(tailinfo ${SAMTOOLS_INSTALL_LOG})" \
        && { [[ check_gcc = "no" ]] && { gcc_error_message; exit 1; } } \
        || install_samtools > $SAMTOOLS_INSTALL_LOG 2>&1 && log "Done, see ${SAMTOOLS_INSTALL_LOG}.";
    } || log "skipping samtools installation"

# bcftools installation ----------------------------------------------
echo "${TO_INSTALL}" | grep "bcftools" > /dev/null \
    && {
        log "Installing bcftools version ${USE_BCFTOOLS_VERSION} to ${BCFTOOLS_PATH}. $(tailinfo ${BCFTOOLS_INSTALL_LOG})" \
        && { [[ check_gcc = "no" ]] && { gcc_error_message; exit 1; } } \
        || install_bcftools > $BCFTOOLS_INSTALL_LOG 2>&1 && log "Done, see ${BCFTOOLS_INSTALL_LOG}.";
    } || log "skipping bcftools installation"


# Miniconda installation ---------------------------------------------
if [[ "${INSTALL_MINICONDA}" == 1 ]]; then
    log "Installing Miniconda to ${MINICONDA_DIR}. $(tailinfo ${MINICONDA_INSTALL_LOG})"
    if [[ $SYSTEM_TYPE = "mac" ]]; then
        url="http://repo.continuum.io/miniconda/Miniconda-${MINICONDA_VERSION}-MacOSX-x86_64.sh"
    else
        url="http://repo.continuum.io/miniconda/Miniconda-${MINICONDA_VERSION}-Linux-x86_64.sh"
    fi

    # Only download and install if it doesn't already exist.
    if [[ ! -e ${MINICONDA_DIR} ]]; then

        install_miniconda () {
            downloader "${url}" miniconda.sh
            bash miniconda.sh -b -p "${MINICONDA_DIR}"
            hash -r
            ${MINICONDA_DIR}/bin/conda config --set always_yes yes
            ${MINICONDA_DIR}/bin/conda update conda

        }

        install_miniconda > $MINICONDA_INSTALL_LOG
        log "Done installing miniconda, see ${MINICONDA_INSTALL_LOG}."

    else
        log "${MINICONDA_DIR} exists -- using existing installation there, and environment ${ENVNAME}."
    fi

    # Set up environment
    log "Setting up isolated Python environment. $(tailinfo $ENV_INSTALL_LOG)"
    if [[ -e "${MINICONDA_DIR}/envs/${ENVNAME}" ]]; then

        # If it exists, activate then install
        log "Activating existing environment."
        source "${MINICONDA_DIR}/bin/activate" "${ENVNAME}"
        log "Installing python dependencies into existing environment. $(tailinfo ${ENV_INSTALL_LOG})"
        # Installing pip will automatically install setuptools along with it
        conda install \
            pip matplotlib numpy scipy nose \
            > $ENV_INSTALL_LOG \
        && log "Done, see ${ENV_INSTALL_LOG}" \
        || { log "Error installing prerequisites, please see ${ENV_INSTALL_LOG}"; exit 1; }
    else

        # Otherwise create and then activate
        log "Installing python dependencies into a new environment. $(tailinfo ${ENV_INSTALL_LOG})"
        ${MINICONDA_DIR}/bin/conda create -n "${ENVNAME}" \
            pip matplotlib numpy scipy nose \
            > $ENV_INSTALL_LOG \
        && log "Done, see ${ENV_INSTALL_LOG}" \
        && log "Activating new environment." \
        && source "${MINICONDA_DIR}/bin/activate" "${ENVNAME}" \
        || { log "Error installing prerequisites, please see ${ENV_INSTALL_LOG}"; exit 1; }
    fi

    echo "# Added by gaav installation, $(date)" > ${INSTALL_DIR}/miniconda-paths
    echo "export PATH=\$PATH:${MINICONDA_DIR}/bin" >> ${INSTALL_DIR}/miniconda-paths

    log "Done, see ${ENV_INSTALL_LOG}."
else
    log "The -M option was used, so skipping Miniconda installation."
fi


    # gaav installation
    log "Installing Python dependencies for gaav and gaav itself.  $(tailinfo ${GAAV_INSTALL_LOG})"

if [[ ${GIT_TAG} = "disable" ]]; then
    log "used -g=disable, so not installing gaav"

else
    if [[ $(check_git) = "no" ]]; then
        echo "
        git does not appear to be installed, so the gaav bitbucket repository
        cannot be cloned.

        Exiting!
        "
        exit 1
    fi
    log "Cloning gaav repository"
    git clone https://shivateja@bitbucket.org/shivateja/gaav.git ${INSTALL_DIR}/gaav \
    && log "Checking out ${GIT_TAG} and installing" \
    && ( cd ${INSTALL_DIR}/gaav && git checkout $GIT_TAG && pip install . > ${GAAV_INSTALL_LOG} ) \
    && log "Done, see ${GAAV_INSTALL_LOG}" \
    || { log "Error installing gaav from git clone, see ${GAAV_INSTALL_LOG}"; exit 1; }
fi


# Each installation process wrote the installed path to this file.  Source it
# now, so we can confirm that the tools were found.  Note that at this point
# the miniconda env should still be activated.
source "${INSTALL_DIR}/paths"

README="${INSTALL_DIR}/README.txt"
cat /dev/null > ${README}

cat >> ${README} <<EOF
Results
-------
Before running this script, the following programs were detected on your path:

$INITIAL


After running this script the following programs can now be detected:

$(check_all)
EOF


[[ $SYSTEM_TYPE = "mac" ]] && profile="${HOME}/.bash_profile" || profile="${HOME}/.bashrc"


if [[ $(grep "export" ${INSTALL_DIR}/paths) ]]; then
    cat << EOF

    I can automatically add the locations of the installed programs to the
    beginning of your \$PATH, which will allow these programs to be
    found when you open a new terminal. This is a good idea if you're not
    comfortable with editing your \$PATH.  Specifically, I can add the contents
    of ${INSTALL_DIR}/paths to the end of your ${profile} file.
    You can always delete these lines later.

    Do you want me to add these programs to your path now?
EOF
        echo -n "(yes/no): "
        read ans

        if [[ ($ans != "yes") && ($ans != "Yes") && ($ans != "YES") &&
            ($ans != "y") && ($ans != "Y") ]]; then

            # If not added automatically here,, then write instructions to the
            # README
            cat >> ${README} <<EOF
Add installed programs to PATH
------------------------------
The paths to these installed programs have been written to the file
${INSTALL_DIR}/paths.  In order to permanently install them, you will need to
add these directories to your ${profile} file.  Pasting this line in a terminal
should do the trick:

    cat ${INSTALL_DIR}/paths >> ${profile}

Then open a new terminal window to activate the changes.

EOF
        else
            # Otherwise do it and then log the changes to the README.
            cat ${INSTALL_DIR}/paths >> ${profile}
            cat >> ${README} << EOF
[ $(date) ]: The following lines were appended to the end of ${profile}:

$(cat ${INSTALL_DIR}/paths)

EOF
            log "Added paths to ${profile}"
        fi # end if y/n
fi # end if something was installed

if [[ ${INSTALL_MINICONDA} = 1 ]]; then
    cat << EOF

    Would you like to prepend the location of the Miniconda installation to
    your \$PATH? This will make it easier to activate enivronments.  This is
    a good idea  if you're not comfortable with editing your \$PATH.
    Specifically, I can add the contents of
    ${INSTALL_DIR}/miniconda-path to the end of your ${profile} file.
    You can always delete these lines later.

    Do you want to add the Miniconda installation to your path now?
EOF
        echo -n "(yes/no): "
        read ans

        if [[ ($ans != "yes") && ($ans != "Yes") && ($ans != "YES") &&
            ($ans != "y") && ($ans != "Y") ]]; then

            # If not added automatically here,, then write instructions to the
            # README
            cat >> ${README} <<EOF


Using the miniconda installation
--------------------------------
A miniconda installation is now at ${MINICONDA_DIR}. To make this permanently
available, please paste the following line in a terminal, which will prepend
the miniconda installation to your PATH:

    cat ${INSTALL_DIR}/miniconda-paths >> ${profile}

Then open a new terminal to activate the changes.
EOF
        else
            # Otherwise do it and then log the changes to the README.
            cat ${INSTALL_DIR}/miniconda-paths >> ${profile}
            cat >> ${README} << EOF
[ $(date) ]: The following lines were appended to the end of ${profile}:

$(cat ${INSTALL_DIR}/miniconda-paths)

EOF
            log "Added miniconda path to ${profile}"
        fi # end if y/n

    cat >> ${README} <<EOF

    Using the new environment
    -------------------------
    An isolated Python environment has been created called ${ENVNAME}.

    When you are ready to use it, open a new terminal and use the command:

        source activate ${ENVNAME}

    Now you will be using the isolated Python environment; anything you do will not
    touch the system-wide installation. When you're done, use

        source deactivate

    to return to normal.

    download the example data with:

        download_metaseq_example_data.py

    and follow the tutorial at https://pythonhosted.org/metaseq/example_session.html

EOF

fi # end if miniconda installed

log "

    Done!  The results of this installation have been written to:

        ${README}

    Please follow the instructions in that file to complete the installation.
"
