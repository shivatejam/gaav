#from ez_setup import use_setuptools
#use_setuptools()

from setuptools import setup, find_packages

setup(
    # Application name:
    name='gaav',
	description="Genome assembly automatic validation"
    # Version number (initial):
    version='0.1',

    # Application author details:
    author='Teja',
    author_email='name@addr.ess',
    
    license='HIPS',

    # Packages
    packages=find_packages(),
    
    scripts=['bin/gaav'],
    )
