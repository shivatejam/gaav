import numpy as np

def parse():

	'''This function parses snps from VCF (variant call format) file'''

	with open( 'snps.flt.vcf', 'rb') as f:
		for line in f:
			if line.startswith('#'):
				continue
			line = line.split()
			snp_pos = int(line[1])
			del line
			yield snp_pos


	    
def snps(sequenceLen):
	
	'''This function assigns 1 for snp present genomic position and zero for snp absent genome position'''
	
	pos = np.fromiter(parse(),dtype=np.int32) #Create a array of snps from an iterable object
	rep = np.repeat(1,len(pos)) #Assign ones for all the snp present genomic positions
	snp = np.zeros(sequenceLen) #All the genomic positions are filled with zeros
	np.put(snp,pos,rep) #Replaces snp present genomic positions of an array with given ones
	del pos, rep
	return snp


