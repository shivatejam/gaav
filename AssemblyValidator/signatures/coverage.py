import numpy as np 
import csv

def analysis():
	
	'''This function calculates standard scores of fragment coverage at each base'''
	
	fragment_coverage = np.loadtxt('ztest.csv', dtype=np.int32, delimiter=',', skiprows=0, usecols=(1,)) #Parse fragment coverage at all bases as numpy array
	
	mean_fragment_coverage = np.mean(fragment_coverage)
	sd_fragment_coverage = np.std(fragment_coverage)
	
	with open('standard_scores.csv', 'wb') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		for coverage_at_base in fragment_coverage:
			standard_score = (coverage_at_base - mean_fragment_coverage)/sd_fragment_coverage
			writer.writerow([standard_score])
	del fragment_coverage
			


