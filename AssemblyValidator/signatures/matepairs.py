from __future__ import print_function
import os
import sys
import csv
import array    
from collections import OrderedDict
import numpy as np  
import scipy.stats




"""This is the "matepairs.py" module"""




def genome_size():
    
    """This function takes a SAM file as command line argument. 
    Each line in SAM file is (recursively) tested for lines start with '@SQ' and parses Genome size.""" 

    with open('aln-pe.sorted.sam', 'rb') as f:
        sequence_length = 0
        for line in f:
            if line.startswith('@'):
                if not line.startswith('@SQ'):
                    continue
                line = line.split('LN:')
                sequence_length += int(line[1])
            else:
                break
        return sequence_length
    



def reference_id():

    """This function takes a SAM file as command line argument.
    Each line in SAM file is (recursively) tested for lines start with '@SQ' and parses reference sequences name and length."""
    
    with open('aln-pe.sorted.sam', 'rb') as f:
        seq = OrderedDict()
        for line in f:
            if line.startswith('@'):
                if not line.startswith('@SQ'):
                    continue
                line = line.split()
                ref_id = line[1].split('SN:')
                reference_length = line[2].split('LN:')
                seq[ref_id[1]] = int(reference_length[1])
            else:
                break
        return seq.items()
    



def parser():
    
    """This function takes a SAM file as command line argument .
    Each line in SAM file is (recursively) tested for seven mandatory fields and parses them."""

    with open('aln-pe.sorted.sam', 'rb') as f:
        s = set()
        for line in f:
            if line.startswith('@'):
                continue
            line = line.split()
            qname = line[0]
            flag = int(line[1])
            rnext = line[6]
            
            if not (flag == 83 or flag == 163 or flag == 99 or flag == 147 or flag == 97 or flag == 145 or flag == 81 or flag ==161) : #consider reads only with these flags
                continue
            if not rnext == "=" :  #Consider reads aligned to same reference
                continue
            if qname not in s:  #skip mates of read pairs and also multiple hits
                s.add(qname)
                record = {		
                    'QNAME' : line[0],			#Parse the fields from SAM file
                    'FLAG' : int(line[1]),
                    'RNAME' : line[2],
                    'POS'   : int(line[3]),
                    'RNEXT' : line[6],
                    'PNEXT' : int(line[7]),
                    'TLEN'  : int(line[8])
                    }
                if record['POS'] < record['PNEXT']: # Add a new field,'POS_END'
                    record['POS_END'] = record['POS'] + (abs(record['TLEN'])-1)
                elif record['PNEXT'] < record['POS']:
                    record['POS_END'] = record['PNEXT'] + (abs(record['TLEN'])-1)
                elif record['POS'] == record['PNEXT']:
                    record['POS_END'] = record['POS'] + (abs(record['TLEN'])-1)
                yield record            




def matepairs_file():
	
	"""This function saves the matepairs data to a file."""
	
	print('writing matepairs data to a file...........')
	
	with open('matepairs.sam','wb') as f:
		f.write('*SAM file with a new addtional field POS_END\n')
		f.write('*Column 1: QNAME\n*Column 2: FLAG\n*Column 3: RNAME\n*Column 4: POS\n*Column 5: RNEXT\n*Column 6: PNEXT\n*Column 7: TLEN\n*Column 8: POS_END\n')
		for dic in parser():
			fields = dic['QNAME'], dic['FLAG'], dic['RNAME'], dic['POS'], dic['RNEXT'], dic['PNEXT'], dic['TLEN'], dic['POS_END']
			fields = '\t'.join(str(field) for field in fields)
			print(fields,file=f)
		
 
 
 
def matepairs():
	
	"""This function reads the matepairs data."""
	matepairs_file()
	with open('matepairs.sam', 'rb') as f:
		for line in f:
			if line.startswith("*"):
				continue
			line = line.split()
			RNAME = line[2]
			POS = int(line[3])
			TLEN = abs(int(line[6]))
			POS_END = int(line[7])
			yield RNAME,POS,POS_END,TLEN

 
 
            
def lib_insertsize():
    #library insertsize distribution
    return (tup[3] for tup in matepairs())
    
            
    

def convergent_lib_mean(data, m=3):
    
    """This function calculates the convergent mean of library insert sizes."""
    
    data = np.fromiter(data,dtype=np.int32)
    approximate_mean = data.mean(0)
    
    while True:			
        data = data[abs(data - np.mean(data)) < m * np.std(data)] #remove outliers from 3 standard deviations
        better_mean = data.mean(0)
        if abs(approximate_mean - better_mean) < 0.001: #iterate for convergence
            return better_mean, data.std(0), data.shape[0]  #returns convergent mean, standard deviation and no.of observations
            break
        approximate_mean = better_mean




def pos_mean(data, m=3):
    
    """This function calculates the mean of insert sizes of inserts those span this base position)."""
    
    data = np.array(data,dtype=np.int32)
    return data.mean(0), data.std(0), data.shape[0] 
    
    


def ztest(local_mean, local_count, global_mean, global_sd):
    
    '''Test for mean based on normal distribution'''
    
    standard_error = global_sd/np.sqrt(local_count)
    zstat = (local_mean - global_mean)/standard_error
    pvalue = scipy.stats.norm.sf(np.abs(zstat))*2
    return zstat, pvalue



def csv_file():
    
    """This function opens a file to store ztest results. Make sure of closing the file eventually."""
    
    print('Writing ztest results to a file............')

    csvfile = open('ztest.csv','wb')
    writer = csv.writer(csvfile, delimiter=',')
    return writer
	



def validation():
   
    global_mean, global_sd, global_count = convergent_lib_mean(lib_insertsize()) #convergent mean, standard deviation, no.of observations
    base = {} #preparing local distributions
    SN,LN = zip(*reference_id()) # reference sequences name(s) and length(s)
    l,m,n,o = 0,1,2,3 #l=ref_id, m=Qstart, n=Qend, o=insert_size
    writer = csv_file()#open a file for writing ztest results
    
    pos=1
    for t in matepairs():
        if t[o] < global_mean-3*global_sd or t[o] > global_mean+3*global_sd:
            continue
        if SN.index(t[l]) == 0: #check mate pair belongs to contig 1
            window_numbera = t[m]
            window_numberb = t[n]
        else:
            value = sum(LN[0:SN.index(t[l])])  #otherwise add length of contig(s)[1...N-1] to successor contig(s)[2...N] 
            window_numbera = t[m]+value		   #to assign genomic position to inserts those span the successor contig(s) if any
            window_numberb = t[n]+value
        #spanning windows (add insert size to insert spanning bases)
        for i in xrange(window_numbera, window_numberb+1):
            base.setdefault(i, array.array('i')).append(t[o])
        while t[m] != pos: #calculate z-test scores 
            print('z-score for base',pos,'calculated')
            try:
                local_mean, local_sd, local_count = pos_mean(base[pos])
                base[pos] = ztest(local_mean, local_count, global_mean, global_sd)
                writer.writerow([t[l]]+ [pos]+ [base[pos][0]]+ [base[pos][1]])
                base[pos] = ()
                del base[pos]
                pos += 1
            except:
                writer.writerow([t[l]]+ [pos]+ [0]+ [0])
                pos += 1
    for pos in base.keys(): #calculate z-test score
        print('z-score for base',pos,'calculated')
        local_mean, local_sd, local_count = pos_mean(base[pos])
        base[pos] = ztest(local_mean, local_count, global_mean, global_sd)
        writer.writerow([t[l]]+ [pos]+ [base[pos][0]]+ [base[pos][1]])
        base[pos] = ()
        del base[pos]
    del base


