import numpy as np
import math
from signatures import microheterogeneities as sm
		
		
	
def assembly(winSize=1000,step=1000):
	
	"""This function validates sequence assembly by sliding genome with window size 1000 base-pairs 
	and finds presence of mis-assembly signatures if any in those window(s) and assign the window(s) as 'suspicious'"""
    
    #########Mis-assembly signatures#########
	matepairs = np.loadtxt('ztest.csv', delimiter=',', usecols=(2,)) #signature
	lenofSequence = len(matepairs)
	coverage = np.loadtxt('standard_scores.csv') #signature
	snp = sm.snps(lenofSequence) #signature

	########sliding window########
	numOfChunks = int(math.ceil((lenofSequence - winSize)/float(step)))+1
	suspiciousWindows = set()
	
	#########output in gff file format########
	with open('output.gff', 'wb') as f:
		f.write('##gff-version 3')
		############validation############
		for i in xrange(0,numOfChunks*step,step):
			if sum(snp[i:i+winSize]) >= 5:
				suspiciousWindows.add(i)
			for z_value, standard_score, snp_value in zip(matepairs[i:i+winSize], coverage[i:i+winSize],snp[i:i+winSize]):
				if (z_value<-3 and snp_value==1 and (standard_score<-3 or standard_score>3)) or (z_value<-3 and snp_value==1) or (snp_value==1 and(standard_score<-3 or standard_score>3)) or (z_value<=-10):
					suspiciousWindows.add(i)
	print sorted(suspiciousWindows)  
    

