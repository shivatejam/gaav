import subprocess
import sys
import glob
import os


"""This is the mem.py module and provide functions for mapping raw data to reference genome."""


def bwa_mem_alignment(reference, reads, mates, num):
	
	'''This function aligns the query sequences (paired-end reads) to the reference (assembly) with BWA-MEM algorithm and generates sam file'''
    
	try:
		subprocess.check_call(['bwa', 'index', reference]) #Construct FM-index for the reference genome
	except OSError:
		print "BWA aligner is not found \nPlease install required version \nsee --help for details."
		sys.exit()
	except subprocess.CalledProcessError: 
		sys.exit()
	else:
		try:
			with open('aln-pe.sam','wb') as sam_file:
				if mates == None:  #Check for mates file and proceed to alignment
					subprocess.check_call(['bwa', 'mem', '-P', '-t', num, reference, reads], stdout=sam_file)
				else:
					subprocess.check_call(['bwa', 'mem', '-t', num, reference, reads, mates], stdout=sam_file)
		except subprocess.CalledProcessError:
			sys.exit()
		except:
			raise


def sam_to_bam(reference):
	
	'''This function imports sam to bam using samtools.
	Samtools is a set of utilites that manipulate alignments in the BAM format'''
	
	try:
		with open('aln-pe.bam', 'wb') as bam_file:
			try:
				subprocess.check_call(['samtools', 'view', '-bS', 'aln-pe.sam'], stdout=bam_file) #Import SAM to BAM when @SQ lines are present in the header        
			except subprocess.CalledProcessError:
				try: 
					subprocess.check_call(['samtools', 'faidx', reference]) #If SQ lines are absent, Where reference.fasta.fai is generated automatically by the 'faidx' command
					subprocess.check_call(['samtools', 'view', '-bt', glob.glob(reference + '*.fai')[0], 'aln-pe.sam'], stdout=bam_file)
				except subprocess.CalledProcessError:
					sys.exit()
			except OSError:
				print "samtools not found \nPlease install required version \nsee --help for details."
				sys.exit()
	except:
		raise


def sorted_sam():
	
	'''This function sort alignments by leftmost coordinates using samtools and returns sorted sam file'''
    
	try:
		subprocess.check_call(['samtools', 'sort', 'aln-pe.bam', 'aln-pe.sorted']) #File <out.prefix>.bam will be created
		with open('aln-pe.sorted.sam', 'wb')as sorted_sam_file:
			subprocess.check_call(['samtools', 'view', '-h', 'aln-pe.sorted.bam'], stdout=sorted_sam_file) #Import sorted bam to sorted sam
	except subprocess.CalledProcessError:
		sys.exit()
	except:
		raise
		


def mem(reference, reads, mates, num):
	
	'''This function call all the above alignment, conversion, sort functions, and returns sorted sam file'''
	
	try:
		bwa_mem_alignment(reference, reads, mates, num)
		sam_to_bam(reference)
		sorted_sam()
	except:
		raise
	
	
