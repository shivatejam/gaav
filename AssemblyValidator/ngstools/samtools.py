import subprocess
import glob
import sys
import os

"""This is the calling.py module and provide functions for snp calling."""

def pileup(reference):
	
	'''This function pipes pileup format of the sorted bam file for snp calling '''

	try:
		try:
		 assert os.path.exists(glob.glob(reference + '*.fai')[0]) #Check the existence of reference index file
		except AssertionError:
			subprocess.check_call(['samtools', 'faidx', reference]) #otherwise create index file
		except subprocess.CalledProcessError:
			sys.exit()
	except:
		raise
	finally:
		try: #First pileup the reads 
			mpileup = subprocess.Popen(['samtools', 'mpileup', '-Iuf', reference, 'aln-pe.sorted.bam'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		except:
			print "Unexpected error:", sys.exc_info()[0]
			#raise
	return mpileup.stdout


	
def snp_calling(reference):
	
	'''This function call snps from the sorted bam file using bcftools. 
    BCFtools is a set of utilites fot the variant calling and manipulating Binary call Format(BCF) and VCF'''
    
	try: 
		with open('snps.raw.vcf', 'wb') as raw_snps_file, open('snps.flt.vcf', 'wb') as filter_snps_file :
			bcf= subprocess.Popen(['bcftools', 'call', '-mv'], stdin=pileup(reference), stdout=raw_snps_file)
			bcf.wait()
			subprocess.Popen(['bcftools', 'filter', '-e', '%QUAL<100', 'snps.raw.vcf'], stdout=filter_snps_file) #Filter snps with quality score >= 100 
	except OSError:
		print "The system cannot find bcftools"
		sys.exit()
	except:
		raise
