from ngstools import bwa
from ngstools import samtools
from signatures import matepairs
from signatures import coverage
import validation


import argparse
import textwrap

def parse_args():
	
	'''This function parses the command line arguments'''
	
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description='Genome Assembly Automatic Validation', 
		epilog=textwrap.dedent('''\
		prerequisites:
		  bwa version 0.7.x
		  samtools version 1.0  
		  bcftools version must be same as samtools version
		'''))
	parser.add_argument( 'reference', help='reference file or Sequence assembly file (fasta)' )
	parser.add_argument( 'reads', help='Illumina paired-end reads (fastq)' )
	parser.add_argument( 'mates', nargs='?', help='If mates file is absent, this program assumes the 2i-th and the (2i+1)-th read in reads file constitute a read pair (fastq)' )
	parser.add_argument( '-num', metavar='INT', default='1', help='number of threads[1]')
	return parser.parse_args()



kwargs = parse_args().__dict__  #Parse command line arguments
	
bwa.mem(**kwargs) #bwa mem alignment
samtools.snp_calling(kwargs['reference']) #samtools SNP calling
matepairs.validation() #ztest
coverage.analysis() #standard scores
validation.assembly() #assembly validation based on mis-assembly signatures 
